"""
Convert a Bible commentary in Markdown into RTF files for import into
Accordance.

Reads a commentary in Markdown that has verse references indicated by
third level headers (###), and outputs one RTF file per verse reference.

"""

import re
import subprocess
import argparse
import logging, sys
import os

def convert_file(state, filename):
     md = open(filename, encoding='utf-8')

     outfile = None
     prolog = ''
     for line in md:
        # Look for headers
        m = re.match(r'^(#+)\s+(.*)',line)
        if m:
            level = len(m.group(1))
            if level <= 3:
                # Close any existing file
                if outfile:
                    outfile.close()
                    outfile = None
            if level == 3:
                # Open a new file, deriving the filename from the verse
                # reference. If there is a range of verses, the first
                # verse number will be used.
                m2 = re.match(r'(\w+)\s+(\d+):(\d+)', m.group(2))
                if not m2:
                    logging.warn('Expected a verse reference in {}". Skipping content.'.format(line))
                    continue
                if state['markdown_only']:
                    outname = "{}/{} {}_{}.md".format(state['output_dir'], m2.group(1), m2.group(2), m2.group(3))
                    logging.info('creating {}'.format(outname))
                    outfile = open(outname, 'w', encoding='utf-8')
                else:
                    outname = "{}/{} {}_{}.rtf".format(state['output_dir'], m2.group(1), m2.group(2), m2.group(3))
                    logging.info('creating {}'.format(outname))
                    args = ['pandoc', '-f', 'markdown', '--standalone', '-o', outname]
                    if state['resource_path']:
                        args += ['--resource-path', state['resource_path']]
                    for filter in state['lua_filter']:
                        args += ['--lua-filter', filter]
                    if state['template']:
                        args += ['--template', state['template']]
                    proc = subprocess.Popen(args, stdin=subprocess.PIPE, encoding='utf-8')
                    outfile = proc.stdin
                outfile.write(prolog)
                prolog = ''
            elif level > 3:
                # Include header in note.
                if outfile:
                    outfile.write(line)
                else:
                    prolog += line
        else:
            if outfile:
                outfile.write(line)
            else:
                prolog += line


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('input', nargs='+')
    parser.add_argument('-o', '--output-dir', dest="output_dir", default=".")
    parser.add_argument('-f', '--lua-filter', dest="lua_filter", action='append', default=[], help='pandoc Lua filter to apply in the conversion of each verse note')
    parser.add_argument('-m', '--markdown-only', dest="markdown_only", action='store_true', help='For debugging: write Markdown that would have been passed to pandoc instead of converting to RTF.')
    parser.add_argument('-p', '--resource-path', dest="resource_path", default=None, help='path for resources (e.g., images) referenced in input files')
    parser.add_argument("-q", "--quiet", dest="quiet_count", action="count", default=0, help="decreases log verbosity for each occurrence.")
    parser.add_argument('-t', '--template', default=None, help='RTF template file')
    parser.add_argument("-v", "--verbose", dest="verbose_count", action="count", default=0, help="increases log verbosity for each occurrence.")
    args = parser.parse_args()

    # A base value of 3 rather than 2 will not print info by default.
    level = max(3 + args.quiet_count - args.verbose_count, 0) * 10
    logging.basicConfig(stream=sys.stderr, level=level)

    state = {
        'output_dir': args.output_dir,
        'lua_filter': args.lua_filter,
        'markdown_only': args.markdown_only,
        'resource_path': args.resource_path,
        'template': args.template
    }
    # Create the output directory if it does not exist.
    os.makedirs(state['output_dir'], exist_ok=True)

    for filename in args.input:
        convert_file(state, filename)

if __name__ == "__main__":
    main()
