-- Workarounds for things Accordance RTF import doesn't support:
-- 
-- * Fake small caps.
-- * Add blank lines between paragraphs.
-- * Place footnotes in small type after the paragraph that contained them.


-- Use 4 non-breaking spaces to indent.
local indent = "\u{a0}\u{a0}\u{a0}\u{a0}"

-- Raw RTF to select a small font for footnotes (12pt, assuming template 16pt)
local small_type = "\\fs24 "


-- Since small caps are not supported by Accordance's RTF import, fake
-- it with ALL CAPS.
function all_caps(el)
  return pandoc.walk_inline(el, {
          Str = function(el)
              return pandoc.Str(pandoc.text.upper(el.text))
          end })
end  


-- Because Accordance does not support interparagraph spacing, add a
-- line break to the end of each paragraph except the last block.
-- We work from top-level paragraphs so we know which is the last block.
function space_paragraphs(doc)
  for i,el in pairs(doc.blocks) do
    if i < #doc.blocks then
      -- It's not the last block.
      if el.t == "Para" then
        el.content:insert(pandoc.LineBreak())
      else -- apply to any paragraphs in block
        doc.blocks[i] = pandoc.walk_block(el, {
          Para = function(elem)
            elem.content:insert(pandoc.LineBreak())
            return elem
          end
        })
      end
    end
  end
  return doc
end


-- Indent block quotes.
function indent_block(el)
  local bq = pandoc.walk_block(el, {
    Para = function(el)
      -- Add indent to the beginning of the paragraph.
      table.insert(el.content, 1, pandoc.Str(indent))
      return el
    end,
    LineBreak = function (el)
      -- Add indent after every line break.
      return {el, pandoc.Str(indent)}
    end
  })
  return bq
end


function place_footnotes(el)
  -- Remove footnotes, placing their content after the Paragraph in small print.
  local footnotes = pandoc.List()
  -- Replace each footnote with a marker.
  local para = pandoc.walk_block(el, {
    Note = function (el)
      footnotes:insert(el)
      return {pandoc.Superscript({pandoc.Str(#footnotes)})}
    end
  })

  if #footnotes > 0 then
    -- Build and return a list of blocks.
    local blocks = pandoc.List({para})
    for num, note in pairs(footnotes) do
      for i, el in pairs(note.content) do
        local prefix = pandoc.List()
        if i == 1 then
          -- Insert footnote number before first block (usually Para).
          prefix:insert(pandoc.Superscript({pandoc.Str(num)}))
          prefix:insert(pandoc.Space())
        end
        -- Insert raw code for smaller text.
        prefix:insert(1, pandoc.RawInline('rtf', small_type))
        prefix:extend(el.content)
        el.content = prefix
        blocks:insert(el)
      end
    end
    return blocks
  end
end


return {
  {Para = place_footnotes},
  {
    BlockQuote = indent_block,
    SmallCaps = all_caps,
    Pandoc = space_paragraphs
  }
}
