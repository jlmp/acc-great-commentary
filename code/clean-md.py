#!/usr/bin/env python3
"""
This script is only meant to be used for the initial import. It tries to
automate as much as possible of the cleanup and enhancement of the
Markdown produced by the conversion of the source HTML.

It runs as a filter, reading from stdin and writing to stdout, and
requires one argument, which supplies the book and chapter name.

To run doctests, use a command like `python3 -m doctest clean-md.py`
"""
import sys
import re
import argparse

def handle_small_caps(match):
    """
    Helper function used by mark_verse_refs() with re.sub().
    """
    initial = match.group(1)
    rest = match.group(2)
    return '[{}{}]{{.smallcaps}}'.format(initial, rest.lower())


def mark_small_caps(text):
    """Identify and mark words that are supposed to be in small caps.
    They are marked in the Great Commentary in an idiosyncratic way, as
    shown in the example below.
    
    >>> mark_small_caps('W**ORD**')
    '[Word]{.smallcaps}'
    """
    text = re.sub(r'\b([A-Z])\*\*([A-Z]+)\*\*', handle_small_caps, text)
    return text

def clean(text):
    # End italics after punctuation.
    text = re.sub(r'\*([.,:;?!])([\s()])', r'\1*\2', text)
    text = re.sub(r'i\.e\*\.,', r'i.e.,*', text)
    text = re.sub(r'([\s"*])0(\s)', r'\1O\2', text) # OCR error 0 for O
    return text

def mark_verses(text, chapter):
    """
    chapter is the first part of the reference, e.g., "1Corinthians 15"
    """
    pattern = '### ' + chapter + r':\2\n\n\1'
    text = re.sub(r'^(Vers?[.e]s?\s+(\d+))', pattern, text, flags=re.MULTILINE)
    return text

def mark_if_scripture(match):
    """
    Helper function used by mark_verse_refs() with re.sub().
    """
    # Recognized names and abbreviations.
    books = {
        "Gen.", "Ex.", "Lev.", "Num.", "Deut.", "Josh.", "Judg.",
        "Ruth", "Sam.", "Kings", "Chr.",
        "Ezra", "Neh.", "Esth.", "Job", "Psa.", "Prov.", "Eccl.", "Song",
        "Cant.", "Is.", "Jer.", "Lam.", "Ezek.", "Dan.", "Hos.", "Joel",
        "Amos", "Obad.", "Jonah", "Mic.", "Nah.", "Hab.", "Zeph.",
        "Hag.", "Zech.", "Mal.", "Tob.", "Judith", "Wis.", "Sir.",
        "Bar.", "Mac.", "Esdr.", "Man.",
        "4Mac.", "Matt.", "Mark", "Luke", "John", "Acts", "Rom.",
        "Cor.", "Gal.", "Eph.", "Phil.", "Col.", "Th.",
        "Tim.", "Titus", "Philem.", "Heb.", "James",
        "Pet.", "John", "Jude", "Rev.",
        "Apoc.",
        "Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy",
        "Joshua", "Judges", "Samuel", "Chronicles",
        "Nehemiah", "Esther", "Psalm", "Psalms",
        "Proverbs", "Ecclesiastes", "Isaiah", "Jeremiah",
        "Lamentations", "Ezekiel", "Daniel", "Hosea", "Obadiah",
        "Micah", "Nahum", "Habakkuk", "Zephaniah", "Haggai",
        "Zechariah", "Malachi", "Tobit", "Wisdom", "Sirach", "Baruch",
        "Maccabees", "Esdras", "Manasseh",
        "Matthew", "Romans", "Corinthians",
        "Galatians", "Ephesians", "Philippians",
        "Colossians", "Thessalonians", "Timothy",
        "Philemon", "Hebrews", "Peter",
        "Revelation",
        "Exod.", "Levit.", "Numb.", "Kin.", "Chron.",
        "Ps.", "Eccles.", "Canticles",
        "Isa.", "Isai.", "Jerem.", "Ez.", "Zach.",
        "Wisd.", "Ecclus.", "Ecclesiasticus", "Macc.",
        "Matth.", "Mar.", "Lu.", "Luc.", "Jn.",
        "Jo.", "Act.", "Ephes.", "Philip.", "Colos.", "Coloss.", "Thess.", "Thes.",
        "Tit.", "Jas.", "Apocal.", "Apocalypse"
    }
    # Don't warn about these common non-books of Scripture.
    non_books = {"And", "Ant.", "Bell.", "Bk", "Book", "Ch.",
        "Civ.", "Confess.", "Disp.", "Dist.", "Ep.", "Epist.", "Evang.", "Fide", "Gent.", "Hær.", "Hist.", "Hom.", "Lib.",
        "Mor.", "Moral.", "Opusc.", "Orat.", "Quæst.", "Serm.", "Sess.", "Tract.", "Trin.", "Trinit."
    }
    ref = match.group(1)
    book = match.group(4).strip('*')
    if book in books:
        return '[{}]{{.vref}}'.format(ref)
    else:
        if book not in non_books:
            sys.stderr.write('Not recognized as a book of Scripture: "{}"\n'.format(book))
        return ref

def mark_verse_refs(text):
    """Returns input text with Scripture references marked.
    
    >>> mark_verse_refs('')
    ''
    >>> mark_verse_refs('No reference here.')
    'No reference here.'
    >>> mark_verse_refs('Rom. iii. 3')
    '[Rom. iii. 3]{.vref}'
    >>> mark_verse_refs('This is in 1 Cor. vii. 2. And x. 4.')
    'This is in [1 Cor. vii. 2]{.vref}. And x. 4.'
    >>> mark_verse_refs('See Numb. xvii.')
    'See [Numb. xvii.]{.vref}'
    >>> mark_verse_refs('*S. Jn.* xvii. 3')
    '[*S. Jn.* xvii. 3]{.vref}'
    """
    text = re.sub(r'(([1-4]\.?\s+)?(\*?S\.?\s+)?(\*?[A-Z]\w+[.*]*)\s+[clxvi]+\b\.?(\s+[\d\-,\s]*\d)?)', mark_if_scripture, text)
    return text

def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('chapter', help='the file\'s book name and chapter number, e.g., "1Corinthians 15"')
    args = parser.parse_args()
    chapter = args.chapter

    text = sys.stdin.read()
    text = mark_small_caps(text)
    text = clean(text)
    text = mark_verses(text, chapter)
    text = mark_verse_refs(text)
    text = "## {chapter}\n\n### {chapter}:1\n\n{text}".format(chapter=chapter, text=text)
    sys.stdout.write(text)

if __name__ == "__main__":
    main()
