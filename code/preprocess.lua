-- Lua filter for preprocessing. This is for operations that need a
-- larger context than the commentary on a single verse.
--
-- * Links page number markers in this format: "[123]{.pnum}"
--     * At least the first of these needs to have a "format" attribute
--       suitable for formatting the URL to which each page will be
--       linked. Use %d in the place where the page number is to be
--       inserted.
--     * An optional numeric "offset" attribute sets the value to add to
--       page numbers to get the page number used in the link.
--     * If the book contains multiple volumes, an optional "volume"
--       attribute indicates it, and the page marker text will show the
--       volume number as well as the page.

local utf8 = require('lua-utf8')

-- Format string found as an attribute of a page marker span
local format = nil
local offset = 0
local volume = nil
local lastPage = nil

function Span(el)
  if el.classes[1] == "pnum" then
    return handlePageNumber(el)
  elseif el.classes[1] == "vref" then
    return handleVerseRef(el)
  end  
  return el
end

-- Process page number Span
function handlePageNumber (el)
  -- Set format if defined in the attributes.
  if el.attr.attributes.format then
    format = el.attr.attributes.format
    offset = 0
  end
  if el.attr.attributes.offset then
    offset = tonumber(el.attr.attributes.offset)
  end
  if el.attr.attributes.volume then
    volume = tonumber(el.attr.attributes.volume)
    lastPage = nil
  end
  -- Convert span contents to a string and find the page number.
  local s = pandoc.utils.stringify(el)
  local page = utf8.match(s, "(%d+)")
  if page and format then
    page = tonumber(page)
    -- Warn about out-of-sequence page numbers.
    if lastPage and page <= lastPage then
      io.stderr:write(string.format("Warning: page %d follows page %d\n", page, lastPage))
    end
    lastPage = page
    -- Wrap the page number in square brackets and link it to the URL.
    local url = string.format(format, page + offset)
    local content
    if volume then
      content = {pandoc.Str(string.format("[vol. %d, p. %d]", volume, page))}
    else
      content = {pandoc.Str(string.format("[p. %d]", page))}
    end
    return pandoc.Link(content, url)
  end
  return el
end

local bookMap = nil

-- process verse reference Span
function handleVerseRef (el)
  if not bookMap then -- build and save
    -- Map abbreviations for books of Scripture to standard Accordance abbreviations.
    bookMap = {
      ["Exod."]    = "Ex.",
      ["Levit."]   = "Lev.",
      ["Numb."]    = "Num.",
      ["1Kin."]    = "1Kings",
      ["2Kin."]    = "2Kings",
      ["1Chron."]  = "1Chr.",
      ["2Chron."]  = "2Chr.",
      ["Ps."]      = "Psa.",
      ["Isa."]     = "Is.",
      ["Isai."]    = "Is.",
      ["Jerem."]   = "Jer.",
      ["Ez."]      = "Ezek.",
      ["Zach."]    = "Zech.",
      ["Eccles."]  = "Eccl.",
      ["Canticles"]= "Cant.",
      ["Wisd."]    = "Wis.",
      ["Ecclus."]  = "Sir.",
      ["Ecclesiasticus"] = "Sir.",
      ["1Macc."]   = "1Mac.",
      ["2Macc."]   = "2Mac.",
      ["Matth."]   = "Matt.",
      ["Mar."]     = "Mark",
      ["Lu."]      = "Luke",
      ["Luc."]     = "Luke",
      ["Jn."]      = "John",
      ["Jo."]      = "John",
      ["Act."]     = "Acts",
      ["Ephes."]   = "Eph.",
      ["Philip."]  = "Phil.",
      ["Colos."]   = "Col.",
      ["Coloss."]  = "Col.",
      ["1Thess."]  = "1Th.",
      ["2Thess."]  = "2Th.",
      ["1Thes."]   = "1Th.",
      ["2Thes."]   = "2Th.",
      ["Tit."]     = "Titus",
      ["Jas."]     = "James",
      ["Apocal."]  = "Rev.",
      ["Apocalypse"] = "Rev.",
      ["3Esdr."]   = "1Esdr."
    }
    -- Add standard Accordance abbreviations.
    for _,v in pairs({
      "Gen.", "Ex.", "Lev.", "Num.", "Deut.", "Josh.", "Judg.", "Ruth",
      "1Sam.", "2Sam.", "1Kings", "2Kings", "1Chr.", "2Chr.", "Ezra",
      "Neh.", "Esth.", "Job", "Psa.", "Prov.", "Eccl.", "Song", "Cant.", "Is.",
      "Jer.", "Lam.", "Ezek.", "Dan.", "Hos.", "Joel", "Amos", "Obad.",
      "Jonah", "Mic.", "Nah.", "Hab.", "Zeph.", "Hag.", "Zech.", "Mal.",
      "Tob.", "Judith", "Wis.", "Sir.", "Bar.", "1Mac.", "2Mac.",
      "1Esdr.", "Man.", "3Mac.", "2Esdr.", "4Mac.", "Matt.", "Mark",
      "Luke", "John", "Acts", "Rom.", "1Cor.", "2Cor.", "Gal.", "Eph.",
      "Phil.", "Col.", "1Th.", "2Th.", "1Tim.", "2Tim.", "Titus",
      "Philem.", "Heb.", "James", "1Pet.", "2Pet.", "1John", "2John",
      "3John", "Jude", "Rev.", "Apoc.",
      "Genesis", "Exodus", "Leviticus", "Numbers", "Deuteronomy",
      "Joshua", "Judges", "1Samuel", "2Samuel", "1Chronicles",
      "2Chronicles", "Nehemiah", "Esther", "Psalm", "Psalms",
      "Proverbs", "Ecclesiastes", "Isaiah", "Jeremiah", "Lamentations",
      "Ezekiel", "Daniel", "Hosea", "Obadiah", "Micah", "Nahum",
      "Habakkuk", "Zephaniah", "Haggai", "Zechariah", "Malachi",
      "Tobit", "Wisdom", "Sirach", "Baruch", "1Maccabees", "2Maccabees",
      "1Esdras", "Manasseh", "3Maccabees", "2Esdras", "4Maccabees",
      "Matthew", "Romans", "1Corinthians", "2Corinthians", "Galatians",
      "Ephesians", "Philippians", "Colossians", "1Thessalonians",
      "2Thessalonians", "1Timothy", "2Timothy", "Philemon", "Hebrews",
      "1Peter", "2Peter", "Revelation"}) do
      bookMap[v] = v
    end
  end

  -- Use ref defined in the attributes if any.
  local ref = el.attr.attributes.ref
  if ref then
    local book, rest = utf8.match(ref, "(%d?%a+%.?)(.+)")
    if book then
      if bookMap[book] then
        ref = bookMap[book] .. rest
      else
        warnf("Warning: unknown Bible book '%s' in '%s'\n", book, ref)
      end
    end
  else -- try to derive ref from span contents
    -- convert span text to reference
    s = pandoc.utils.stringify(el)
    -- If there's an "S. ", discard it (e.g., "1 S. Peter" instead of "1 Peter.").
    s = utf8.gsub(s, "S%. ", "", 1)
    -- first try with a leading number (as in "1 Kings xv. 3.")
    local booknum, book, chapter, verse = utf8.match(s, "(%d)%.?%s+(%a+%.?)%s+([clxvi]+)%.?%s*([0-9–, -]*)")
    if booknum then
      book = booknum .. book
    else
      -- try without a leading number
      book, chapter, verse = utf8.match(s, "(%a+%.?)%s+([clxvi]+)%.?%s*([0-9–, -]*)")
    end
    if book then
      if bookMap[book] then
        book = bookMap[book]
      else
        warnf("Warning: unknown Bible book '%s' in '%s'\n", book, s)
      end
      chapter = romanToNumber(chapter)
      if verse:len() > 0 then
        -- Replace any en dashes with hyphens.
        verse = verse:gsub("–","-")
        ref = string.format("%s %d:%s", book, chapter, verse)
      else
        -- a whole chapter is cited
        ref = string.format("%s %d", book, chapter)
      end
    else
      io.stderr:write(string.format("Warning: unrecognized Bible ref '%s'\n", s))
    end
  end
  if ref then
    return pandoc.Link(el.content, ref)
  end
  return el
end

-- Print a formatted warning to stderr
function warnf (...)
  io.stderr:write(string.format(...))
end

-- code to read roman numerals, derived from
-- https://gist.github.com/efrederickson/4080372 (Copyright (C) 2012 LoDC)
function romanToNumber(s)
  local map = { 
    I = 1,
    V = 5,
    X = 10,
    L = 50,
    C = 100, 
    D = 500, 
    M = 1000,
  }
  s = s:upper()
  local ret = 0
  local i = 1
  while i <= s:len() do
    local c = s:sub(i, i)
    if c ~= " " then -- allow spaces
      local m = map[c] or error("Unknown Roman Numeral '" .. c .. "'")
      
      local next = s:sub(i + 1, i + 1)
      local nextm = map[next]
      
      if next and nextm then
        if nextm > m then 
          -- if string[i] < string[i + 1] then result += string[i + 1] - string[i]
          -- This is used instead of programming in IV = 4, IX = 9, etc, because it is
          -- more flexible and possibly more efficient
          ret = ret + (nextm - m)
          i = i + 1
        else
          ret = ret + m
        end
      else
        ret = ret + m
      end
    end
    i = i + 1
  end
  return ret
end
