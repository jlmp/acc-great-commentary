-- Check for Markdown errors

function Str (el)
  -- warn if the string contains an asterisk (italics problem).
  if el.text:find('%*') then
    io.stderr:write(string.format("Warning: asterisk in '%s'\n", el.text))
  end
  return el
end
