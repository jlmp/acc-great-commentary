## Luke 21

[470]{.pnum format="https://archive.org/details/thegreatcommenta04lapiuoft/page/%d"}
1 *Christ commendeth the poor widow.* 5 *He foretelleth the destruction
of the temple, and of the city Jerusalem:* 25 *the signs also which
shall be before the last day.* 34 *He exhorteth them to be watchful.*

### Luke 21:18

[471]{.pnum}
Ver. 18.---*But there shall not an hair of your head perish.* "Because,"
says S. Gregory, "what was said about death was hard, comfort is added
at once, from the joy of the resurrection, when it is said, 'a hair of
your head shall not perish.' For we know that the flesh when [472]{.pnum} wounded,
causes pain, but the hair when cut does not. Our Lord therefore said to
His martyrs, 'A hair of your head shall not perish.'" From these words
of Christ, we may conclude that we shall rise again with our actual
bodies. S. Augustine (*De Civitate, chap.* 19, 20.) So S. Bonaventure, S.
Thomas, the master of the sentences, Soto, and others. Their proof is
from [Matt. x. 30]{.vref}: "The very hairs of your head are all numbered;" and
from this of S. Luke, "Not a hair of your head shall perish." "Not in
length," says S. Augustine, "but in number."

2\. We may collect this from reason, for our bodies will rise without
deformity, with their natural adornments and comeliness; the adornment
of the head is the hair, the beard, the nails. If any one has not these
he is a deformed.

### Luke 21:19

Ver. 19.---*In your patience possess ye your souls.* Patience,
therefore, is the possession of our souls. Firstly, because patience
rules the soul and directs it in peace, and bends and influences it as
it pleases. Secondly, because no one can keep the hope of a future life,
as S. Augustine says, unless he have patience in the labours of the
present one. Thirdly, S. Gregory (*Homily* xxxv. *in Evangel.*): "The
possession of the soul consists of the virtue of patience, because
patience is the root and guardian of all virtues. Through patience, we
possess our souls, because, while we learn to govern ourselves, we begin
to possess the knowledge that we are (quod sumus, quod adverb). It is
patience to endure calmly the evils we suffer from others, and to be
affected with no painful feeling against him who inflicts them upon us.
For whoever so takes the oppressions of others, as to grieve in silence,
but to look out for a time of retribution, does not possess this virtue,
but only makes a show of it. Again, Solomon says, [Prov. xvi. 32]{.vref}: 'The
patient man is better than the valiant, and he that ruleth his spirit
than he that taketh cities.' The taking of a city is therefore a less
victory, because the conquest is outside ourselves. That which is
subdued by patience is greater, because the mind is subdued by itself,
and subjects itself to itself when patience subdues it to the humility
of endurance." S. Gregory adds the example of the Abbot Stephen, who
returned contumelies with thanks, and thought a gain, loss, and
considered his adversaries his [473]{.pnum} helpers. Hence, at his death, angels were
seen taking his soul to heaven.

The impatient do not possess their souls, but are possessed by the vices
of wrath and vindictiveness, and consequently by Satan. They, only, who
have ardent love can gain true patience, as those fervent martyrs---SS.
Ignatius, Laurence, Sebastian, Vincent, and others. Trajan the Emperor,
consequently, said when he conferred, by his sentence, martyrdom on S.
Ignatius, "No people suffer so much for their God as the Christians." S.
Gregory (*book* v. *Moral. chap.* 13), "What is it to possess our souls,
but to live perfectly in all things, and to govern all the emotions of
our minds by the art of virtue? Whoever therefore possesses patience,
possesses his soul, because he is thus made strong against all
adversities, so that he rules even by subduing himself. By whatever he
masters himself, he clearly shows himself unmastered, for when he
masters himself in his pleasures, he prepares himself to be unmastered
by their opposites." In his 39^th^ Epistle to Theoclister; "In your
patience possess your souls. Consider a moment where patience would be
if there were nothing to be endured. I suspect that he would not be an
Abel who had no Cain. For if the good were without misfortunes, they
could not be perfectly good, for they would have no purgation. Their
very society with evil is the purification of the good." Hence, says
Theodore Studita in his 19th Catechetical Lecture, "Endurance is the
highest perfection of virtue;" and Lucan (*lib.* ix.):

+-----------------------------------+-----------------------------------+
| ---Serpens, sitis, ardor, arenæ\  | The sandy desert's burning heat;  |
| Dulcia Virtutis, gaudet Patientia | the pangs\                        |
| duris.                            | Of raging thirst; its serpent's   |
|                                   | cruel fangs,\                     |
|                                   | Are Virtue's sweets; for Patience |
|                                   | joys in these,\                   |
|                                   | And welcomes hardships more than  |
|                                   | softest ease.                     |
+-----------------------------------+-----------------------------------+

Lastly, the whole band of virtues flows into patience, so that it
appears to be the complex of all virtues. Seneca (*Ep.* 69. and
following): "There is a fortitude of which the brands are patience, [474]{.pnum}
endurance, and toleration. There is prudence, without which no
undertaking is entered upon, and which persuades us to endure bravely
what we cannot escape. There is constancy which cannot be cast down from
its pedestal, and the determination of which no force can overthrow.
Here is that indivisible society of virtues." And see the words of [S.
James i. 4]{.vref}.

### Luke 21:34

Ver. 34.---*And take heed to yourselves, lest* "the cares of this life
absorb the mind and sink the faculties," says Euthymius, "and do not
allow men to think about their salvation." "The cares of this life,"
says Titus, "debauchery and ebriety, deprive men of their senses,
obscure their faith, and cause forgetfulness of all that is useful and
necessary. They distract the mind, seize hold of it, and absorb it in
the cares of this world."

### Luke 21:35

Ver. 35.---*For as a snare shall it come on all them that dwell on the
face of the whole earth.* As careless birds are taken craftily by
snares, so in the day of judgment shall the men of pleasure be. 2 "As
the snare strangles the birds, so shall the day of judgment choke
sinners." 3. "As the snare always keeps hold," says the Interlinear, "of
that which it has once caught, so shall the sentence, given by one
Christian judge, be perpetual; and either for ever glorify him who is
judged, in heaven, or consume him with fire in hell."

### Luke 21:36

Ver. 36.---*Watch ye therefore, and pray always, that ye may be
accounted worthy to escape all these things that shall come to pass, and
to stand bore the Son of man.* The Arabic has, "That you may be
strengthened in flight."

Stand before the Son of Man. So [Wisdom v. 1]{.vref}: "They shall stand with
great constancy." "To those therefore who give themselves up to vigils,
prayers, and good works, that day shall not be a snare, but a festival,"
says Theophylact.

### Luke 21:37

Ver. 37.---*And in the daytime He was teaching in the temple; and at
night He went out, and abode in the mount that is called the mount of
Olives.* Because olives abounded in it. Christ gave the day to preaching
and to His neighbour, but the night to prayer, to Himself, and to God.
Thus He gave very little time to repose and slumber. [475]{.pnum} The same did S.
Paul, Dominic, F. Xavier, and others like them. "He went by night," says
Theophylact, "into the mountain, to show us that we ought to hold
communion with God in quiet at night. By day we should be gentle, and do
good." So Bede: "What He commanded in words, He confirmed by His own
example; for when the time of His Passion drew near, He was instant in
teaching, in watching, and in prayers, either urging those, for whom He
was to suffer, to faith by His words, or commending them to His Father
by His prayers."

### Luke 21:38

Ver. 38.---*And all the people came early in the morning to Him.* The
senses are in their vigour in the morning, and the morning therefore, as
the best part of the day, is to be given to God.
