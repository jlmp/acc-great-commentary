# Second Epistle to the Corinthians

## 2Corinthians 1

[1]{.pnum format="https://archive.org/details/greatcommentaryo08lapiuoft/page/%d" volume="8"}
3 *The apostle encourageth them against troubles,* *by the comforts and
deliverances which God had given him,* *as in all his afflictions,* 8
*so particularly in his late danger in Asia.* 12 *And calling both his
own conscience and theirs to witness of his sincere manner of preaching
the immutable truth of the gospel,* 15 *he excuseth his not coming to
them, as proceeding not of lightness, but of his lenity towards them.*

#### Contents

[2]{.pnum}
He consoles the Corinthians, whom in the First Epistle he had sharply
rebuked, and absolves the excommunicated fornicator, who was now
penitent. He then proceeds to treat of true repentence, of the dignity
of the ministers of the New Testament, of the duty of avoiding the
company of unbelievers, of patience, of almsgiving for the poor saints
at Jerusalem, of the duty of rejecting the false Apostles who set
themselves up as rivals to S. Paul among the Corinthians, and
depreciated him, and rendered it necessary for him to sing his own
praises in self-defence. Then he threatens some of the Corinthians who
still refused to submit to his apostolic authority. The whole Epistle
may be said to be a defence and laudation of his apostleship. The Greek
MSS., the Syriac, and the Latin Complutensian have a note at the end
that it was written at Philippi in Macedonia, and sent by Titus and
Luke. Baronius, however, thinks that it was written at Nicopolis, A.D.
58, when the Apostle, after being forced to leave Ephesus, where he
wrote his First Epistle, after the uproar raised by Demetrius, left
Timothy as Bishop of Ephesus, and came to Troas; then, not finding Titus
there, he proceeded into Macedonia, and from thence into Greece; thence
he sailed by the Ægean Sea and touched at Crete, where he left Titus. At
length he came to Greece again, to Nicopolis, where he had determined to
winter ([Tit. iii. 12]{.vref}). Cf. Baronius, vol. i. p. 575. It is likely that
he wrote this Epistle there in quietness, but the point cannot be
decided certainly; for S. Paul, while travelling up and down through
Asia, might have gone to and returned from Philippi, and might have
stayed there long enough to write it. S. Luke, as is well known, does
not record all the stoppages or all the journeyings of the Apostle. Cf.
[Acts xx.]{.vref}

#### Synopsis of the Chapter

i. Paul shows, in order that he might console others, from what
great tribulations in Asia the Lord had delivered him.

ii. He commends himself to the Corinthians (ver. 12), by a
declaration of the sincerity of his heart and of his doctrine.

iii. He clears himself (ver. 17) from the charge of lightness and
inconstancy induced by his not coming to them as he had promised, and at
the same time affirms the sure and constant truth of his preaching.

### 2Corinthians 1:1

Ver. 1.*---Timothy our brother.* That is our co-Apostle; so the Pope
calls Bishops his brethren, a Bishop his canons, an abbot his monks.

### 2Corinthians 1:3

Ver. 3.*---The Father of mercies.* A Hebraism for "most merciful." See
note to [Rom. xv. 5]{.vref}.

S. Bernard says learnedly and piously (*Serm.* 5 *de Natali.Dom.*): "*He
is rightly called the Father of mercies, not the Father of judgments or
vengeances, not only because it is more the nature of a father to pity
than to be angry, even as a father pitieth his children that fear him,
but rather because it is from Himself that He draws the cause and origin
of His mercy, but from us, that is, from our sins, draws the cause and
origin of His judgment and vengeance. But if it is because of this that
He is the Father of mercy, why is He called the Father of mercies? The
Apostle in one Word, in one Son, brings before us a double mercy in the
words* '*Father of mercies*,' *not merely Father of a single mercy, in
speaking of the God not of comfort merely, but* '*of all comfort*,' *who
comforteth us, not in this or that tribulation, but in all.* '*Many are
one mercies of the Lord*,' *say a certain person, meaning that many are
the tribulations of the righteous, and the Lord will deliver them out of
all. There is one Son of God, one Word; but our manifold misery calls
for, not only great pity, but a multitude of mercies. Perhaps, however,
because of the double substance which is to be found in our human
nature, both of which are miserable, the misery of man may not
unsuitably be said to be twofold, although in both it be of manifold
forms. Truly the tribulations of our body and soul are increased
exceedingly, but He who saves man wholly rescues him from the troubles
of both.*"

Notice that S. Bernard seems to refer the phrase "Father of mercies" to
the Son, and rightly enough, but it is not the intention of the Apostle
to do so. S. Paul plainly means the same Person by "the Father of our
Lord Jesus Christ, and the Father of mercies."

### 2Corinthians 1:5

Ver. 5.---*For as the sufferings of Christ abound in us, so our
consolation also aboundeth in Christ.* "The sufferings of Christ" are,
(1.) as S. Ambrose takes it, those which we suffer for Christ; (2.) such
as Christ suffered; (3.) those which Christ regards as His own, in
accordance with [S. Matt. xxv. 40]{.vref} and [Acts ix. 4]{.vref}, as Œcumenius
understands the words. Theophylact adds that the word "abound" is used
to point to the fact that Christ suffered more in His members than in
Himself. This is true by way of extension, but not in the way of
intension. In S. Laurence Christ suffered the fire, in S. Stephen the
stones, in Ignatius the wild beasts; but His suffering and sorrow in
Himself were greater and more intense than what all these suffered. The
meaning, therefore, is this, according to Theophylact: Do not be
downcast whoever of you suffers from afflictions and various ills,
because, however great your sufferings may be, so great is your
consolation.

But here observe, (1.) as Theophylact does, that S. Paul does not merely
say that the comfort equals the sufferings, but that it abounds and is
greater than they are; and, therefore, whoever is afflicted may bear his
troubles patiently, nay joyfully and gladly, and so may gain the victory
over them. (2.) The sufferings of Christ have this characteristic, that
Christ gives consolation in proportion to them, and the greater the
suffering the greater the comfort. On the other hand the sufferings of
the world are vinegar without honey, and as they increase, so do
desolation and mourning and woe. (3.) It follows from this that the
suffering of the Cross is not to be fled from but embraced, as the
mother of so much Divine comfort and joy. So S. Andrew, Ignatius, Xavier
embraced it, and prayed daily for the Cross, and would not be set free
from it unless God would give them a heavier one.

### 2Corinthians 1:6

Ver. 6---*And whether we be afflicted, it is for your consolation.* We
suffer tribulations that we may consol and save you, and may animate
you, by our patience and hope in God and His comfort, to bravely bear,
as we do, afflictions on behalf of the faith. So Ambrose. Cf. Chrysostom
(*Hom.* 1 *de Spe et Fort. in Tentat. Serv.*).

Which is effectual in the enduring of the same sufferings. This
salvation, as the wished for end, produces patience. Others, as
Theophylact, take it, "Salvation is wrought in patience." Ambrose takes
it to mean that patience is the meritorious cause of salvation, and that
salvation, therefore, produces patience as its final cause, for the
efficient and final causes have a mutual relation. Salvation, as the
final cause, orders and works patience, and in turn patience, as the
efficient cause, works out salvation. The meaning, then, is that your
consolation and salvation alike effectually produce patience, our
exhortation animates you to hope for salvation, and to bear bravely on
its behalf whatever sufferings arise from obedience to the faith. My
exhortation or consolation, therefore, works effectually endurance by
stirring, you up to it; the salvation thence hoped for works endurance
objectively. Just so the resolution to attain some end makes us lay hold
of and employ means.

### 2Corinthians 1:8

Ver. 8.*---Which came to us in Asia.* From the tumult raised by
Demetrius, recorded in [Acts xix. 29]{.vref}. So S. Thomas understands this
passage, as do all other interpreters except Cajetan, who thinks that
there is a reference here to some persecution not mentioned in
Scripture.

We were pressed out of measure, above strength. Above the strength of
nature, not of grace---more than the body could bear, not the mind; for
by the help of grace Paul bore this tribulation undauntedly and overcame
it. "God is faithful," he says, in [1 Cor. x. 13]{.vref}, "who will not suffer
you to be tempted above that ye are able" to bear by the help of grace.
Moreover, he does not say that he was tempted, but pressed or afflicted
above his strength, inasmuch as the body is a heavy burden, though the
soul preserve her fortitude, and fortitude overcome temptation.

Insomuch that we despaired even of life. Nature would have preferred
death to suffering such afflictions. But there was no despair when the
charity and grace of God were considered, by which Paul was enabled to
bear any afflictions whatever in God's service. This despair or
weariness was felt by many saints. Cf. [Job x. 1]{.vref} and [1 Kings xix. 4]{.vref}. The
Greek word denotes also anxiety and perplexity. Hence Chrysostom renders
it, "We were in doubt," and Vatablus as in the text. Hence follows (ver.
9), "But we had the answer of death in ourselves." The Latin version
gives *tædium,* or weariness.

### 2Corinthians 1:9

Ver. 9.---*But we had the sentence of death in ourselves.* "But," here,
has the meaning of "moreover." Nature and inclination presaged and
expected nothing but death; and when I thought of the state of my life,
my mind answered that I must die if God did not lend miraculous aid. So
Ambrose and Theophylact.

The Greek word here rendered "sentence" means, (1.) answer. (2.)
According to Photius, it denotes the crisis of an illness. The meaning,
then, would be: We were so afflicted that our life was despaired of by
nature and by experienced men, who, looking at our case as doctors
might, judged it beyond recovery. (3.) It denotes sentence, as in the
text. We seemed to have received our sentence, and to be destined
accordingly to inevitable death.

### 2Corinthians 1:10

Ver. 10.---*Who delivered us from so great a death.* "From so great
dangers," according to the Latin. The meaning is the same. Ambrose reads
"from so great deaths." The Hebrews are wont to apply the name of
*death* to great dangers, violent persecutions, grief, and agony that
are akin to death, and that seem to threaten a speedy death. So
Chrysostom. Cf. [Ps. xviii. 5]{.vref}, and [2 Cor. xi. 23]{.vref}.

### 2Corinthians 1:11

Ver. 11.---*That by the means of many persons.* Primasius reads this,
"By a company of many persons," that is, children, youths, and old men.
 S. Paul's meaning is, that through many people in a great concourse of
men, thanks may be publicly given to God for S. Paul's deliverance and
safe return, as the common father and Apostle of all.

For the gift bestowed upon us. That thanks may be given, says Vatablus,
by many, on our behalf, for the gift of grace that was given to us. As
gratitude demands that thanks be given, in proportion to the benefit
bestowed, to the great Giver for our creation, redemption,
justification, education, and growth, so also should thanks be given for
the gift of deliverance.

### 2Corinthians 1:12

Ver. 12*.---For our rejoicing is this, the testimony of our conscience.*
"For" introduces the reason why the Corinthians should give thanks and
pray for Paul. It is because he was their Apostle, who, with great grace
and efficacy, preached to them the Gospel and converted them; and in
proof of this he calls upon his own conscience and theirs.

Observe here the force and quiet that come from a good conscience. "No
theatre," says Cicero, "for virtue is so great as that of conscience."
Juvenal, too (*Sat.* xiii.), says. "The summit of happiness is to have a
mind conscious of its own integrity." S. Augustine again (*contra
Secund. Manich.* c. i.) says: "Think of Augustine what you like, my
conscience shall not be my accuser in the presence of God." See notes to
[1 Tim. i. 5]{.vref}.

Not with fleshly wisdom. I have not preached with human philosophy or
eloquence, but with grace, zeal, efficacy, and the Holy Spirit.

### 2Corinthians 1:14

Ver. 14.---*We are your rejoicing, even as ye also are ours.* We are the
object of your rejoicing as your teachers; ye, as good disciples, are
the object of our rejoicing; and this rejoicing will chiefly be seen in
the day when the Lord will come to judge all men.

### 2Corinthians 1:15

Ver. 15*.---I was minded to come unto you before, that ye might have a
second benefit.* The first benefit was that of his First Epistle; his
second would have been his visit to them in person. So Theophylact. Or
else the first benefit was his first visit, when he converted them; his
second would be his second visit, to confirm them in the faith.

### 2Corinthians 1:16

Ver. 16*.---And to pass by you into Macedonia.* To pay them a flying
visit, and then return from Macedonia to them again, so as to stay
longer with them. This is what he means in [1 Cor. xvi. 5]{.vref}, where he says
that he would come to them after he had passed through Macedonia. Here
he adds further to this that he also wished to see them on his way to
Macedonia. So the Greek Fathers harmonise the passages; but Lyranus and
S. Thomas reconcile them differently, but not so probably.

### 2Corinthians 1:17

Ver. 17.---*When I therefore was thus minded, did I use lightness?* That
is, when I proposed to come to you and did not. The Greek word for
lightness is derived from the word for a stag. In a like way we speak of
the wisdom of the serpent, the innocence of the dove, the stubbornness
of the ass, the headiness of the elephant.

Or the things that I purpose, do I purpose according to the flesh? S.
Paul did not form his determinations relying on human prudence and
lightness, which readily change men's designs, through worldly advantage
or convenience, or the influence of superiors, nay, through the mere
fickleness and changeability of natural inclination. So Ambrose.

That with me there should be yea yea, and nay nay. I was not so unstable
and purposeless as at one time to promise to come and at another to
refuse, as boys often do. So Anselm.

### 2Corinthians 1:18

Ver. 18.---*But as God is true, our word toward you was not yea and
nay.* I call the true God to witness, who is a faithful and true
witness, that in teaching you I did not deceive you, and, therefore,
that it was not my intention to fail you when I promised to come to you.

This teaches the preacher to beware of lightness and fickleness of life,
lest the people infer from it that the truth which he preaches is
equally unfixed and uncertain.

### 2Corinthians 1:19

Ver. 19*.---For the Son of God . . . was not yea and nay, but in Him was
yea.* My preaching and teaching about Christ was not variable,
inconstant, and contradictory, but was a constant, uniform statement,
for I always said and taught the same of Christ.

### 2Corinthians 1:20

Ver. 20*.---For all the promises of God in Him are yea.* All the
promises of God in the Old Testament relating to the Messiah were
constant and true, and have been fulfilled in Him.

1 The yea yea here, and in [S. Matt. v. 47]{.vref}, have a threefold
signification: (1.) constant asseveration, as opposed to inconstancy and
deceit; (2.) truth or reality, as opposed to falsity or unreality; (3.)
simple affirmation, as opposed to an oath. Cf. [S. James v. 12]{.vref}.

*And in Him Amen.* "And therefore we say, Amen" is the Latin rendering;
that is, we affirm that those promises were true. So Chrysostom and
Ambrose. For further notes on "Amen," see [1 Cor. xiv. 16]{.vref}.

Add to this that Amen is usually an adverb denoting truly, firmly,
faithfully, and thence came to be the name of the abstract qualities of
truth, firmness, and faithfulness. Cf. [Isa. lxv. 16]{.vref}; [Jer. xi 5]{.vref}; [Isa.
xxv. 1]{.vref}; [Rev. iii. 14]{.vref}, vii. 12. The meaning, therefore, here is: Through
Him, Christ, the Amen, *i.e.,* truth, faithfulness, and constancy, we
give glory to God, saying: All that God promised concerning Christ is
Amen, *i.e.,* most true, and has been most truly fulfilled by God.

### 2Corinthians 1:21

Ver. 21*.---Now He which stablished us.* Some think that this is an
ellipse, and we must understand the meaning to be, He which stablisheth
us prevented, the execution of my purpose. But it is far better to refer
these words, as others do, to what immediately precedes them. The
promises of God have been fulfilled in Christ; but He who by His power
and authority fulfils them is God Himself: as He promised, so in fact
does He stablish us, anoint us, and seal us in Christ. In the third
place, it would not be amiss to refer these words to what was said in
ver. 18, "Our word toward you was not yea and nay." In other words---I
am not fickle and inconstant in my speech, my preaching, and promises.
It is God who gives me this constancy, and therefore let no one think
that I am arrogant enough to ascribe it to my own strength and
fortitude, since I profess that I have it, not from myself but from God.
As God in Himself and in His promises is yea, that is, is ever constant,
firm, and unchangeable, so does He strengthen us, and make us firm and
constant in the faith and in what we promise.

*And hath anointed us in God, who also hath sealed us, and given the
earnest of the Spirit in our hearts.* This seal, says Calvin, is that
special Divine faith by which each has a certain knowledge that he is
predestinated. But this seal is uncertain and unreliable, and this faith
is false and foolish presumption. For the Apostle, who had as great
faith as possible, fears reprobation in [1 Cor. iv. 27]{.vref}. His Divine faith,
therefore, did not give him certain assurance of his predestination.
Moreover, he frequently impresses on all the faithful that they
carefully work out their own salvation with fear and trembling, and by
so doing he takes from them all ground for assurance of their salvation.
Add to this that no one is certain that he has this Divine faith, or
that he will always have it; nay, many have fallen away from this faith
of Calvin's who before believed with him that they were of the number of
the predestinate.

I say, then, 1. that *God hath sealed* means, He has confirmed His
promises as though He had stamped them with His seal, by giving,
according to them, as a pledge of our future inheritance, His grace, by
which He has sealed and anointed us to be the sons of God, separated
from the sons of the devil. So Chrysostom, Theodoret, Œcumenius. This
seal is altogether certainly known to God, but to us is only a matter of
probability. This establishing, anointing and sealing take place through
one and the self-same grace. Similarly, in Eph. 1. 13 he says that we
have been sealed with the Holy Spirit of promise.

2\. This passage may be referred to baptism; for (*a*) in baptism God
anointed us with the oil of His grace; (*b*) He gave the earnest of the
Spirit in the testimony of a good conscience; (*c*) He sealed us with
the 'character' of baptism. Cf. Bellarmine (*de Effectu. Sacr.* lib. ii.
c. 20). The exposition of Theophylact and Chrysostom is to be referred
to this. They say: "*He hath anointed us and sealed us to be prophets,
priests, and kings.*" Cf. Chrysostom (*Hom.* 3) on these words, who
points out how Christians who govern their passions are kings anointed
by God.

3\. It is the best explanation which refers these words to the sacrament
of Confirmation, which, in olden times, was received by all the faithful
to strengthen them against persecution. S. Paul has expressly
distinguished, "He hath established us," "He hath given the earnest of
the Spirit,". "hath anointed us," "hath sealed us." But these four
things cannot be distinguished anywhere save in the sacrament of
Confirmation.

These words point to four effects of the sacrament of Confirmation: (1.)
The gift of faith, by which we are strengthened in Christ. Hence, as was
said in ver. 18, S. Paul's faithful preaching of Christ was firm and
constant, because God had strengthened him for it in Christ by means of
the sacrament of Confirmation, *i.e.,* through Christ and His merits.
(2.) The second effect is the grace of charity, with which we are
abundantly anointed, as with a spiritual chrism. The Greek, indeed, for
anointed is the very word whence come "Christ" and "Christians," so that
"Christians" are "the anointed ones." Hence S. Augustine (*Serm.* 342)
says: "The word 'Christ' is from chrism, *i.e.,* anointing. Every
Christian, therefore, is sanctified, in order that he may understand
that he not only is made a partaker of the priestly and royal dignity,
but also an adversary of the devil." (3.) The third fruit is the earnest
of the Spirit, which is the testimony of a good conscience given by the
Holy Spirit, and which is as the earnest of the future glory promised,
and to be given by the Holy Spirit. For the sense in which the Holy
Spirit is the pledge or earnest, see notes to [Eph. i. 14]{.vref}. (4.) The
fourth fruit is the seal and sign of the Cross on the forehead,
signifying the "character" imprinted on the soul, by which we are sealed
as His servants, or rather His soldiers and leaders. Cf. Ambrose (*de
his qui Mysteriis Initiantur,* c. vii.), Suarez (pt. iii. qu. 63, art. 1
and 4).

### 2Corinthians 1:23

Ver. 23.---*Moreover, I call God for a record upon my soul.* From this
it is lawful for a Christian to take an oath, says S. Augustine (qu. 5,
inter. 83); for the Apostle here takes an oath, and that one of
execration. If I lie, he says, may God be my judge and condemn my soul.

That to spare you I came not as yet unto Corinth. Lest I should be
forced to exert my apostolic authority against the vices of the
offenders among you: it was to spare you from being grieved by my coming
to correct you. So Anselm. Cf. also chap. ii. 1. S. Paul here gives the
real reason why he had not kept his promise, or his purpose of visiting
Corinth, which was that the Corinthians had not yet given up the vices
of which he had admonished them in his First Epistle, and deserved
therefore to be rebuked still more sharply and punished. But he deals
gently with them, and by his absence he wished tacitly, and by his
Epistle openly to remind them once more of their duty, and so correct
them with gentleness.

Let prelates learn from this not to be ever chiding and rebuking those
under them for their faults, lest they make them hard and callous. And
more than this, the faults of some people, especially those that are
more high-minded and sensitive, are more effectually corrected if they
are pointed out patiently and indirectly than if they are rebuked
openly, or actually visited with punishment. Cf. S. Gregory (*Pastor.*
pt, iii. c. 8 and 9).

*As yet.* That is, after his first visit, or after the First Epistle.

### 2Corinthians 1:24

Ver. 24*.---Not for that we have dominion over your faith, but are
helpers of your joy.* This is a well-known rhetorical figure of speech,
by which he tones down what had been said before of his power. He means:
I said that 1 was unwilling to punish, and wished you of your own accord
to correct yourselves; but I said this not from love of power, or as
though I wished to act arbitrarily, but to improve you, that when you
were so corrected you might rejoice both on earth and in heaven. This
rebuke of mine, therefore, is not so much a rebuke as a support and help
to your joy. So Anselm.

*For by faith ye stand.* "Which," says S. Anselm, "works by love and is
not forced by dominion." In your faith I have nothing to correct, but
only in your actions; and, since you are of the faithful, I will not
imperiously scold you, but gently admonish you by this letter, that so
you may all rejoice with me. Since you are of the faith, I have little
doubt but that you will at once listen to my admonitions.
