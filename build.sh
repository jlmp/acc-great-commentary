#!/bin/sh
rm -rf build/*
mkdir -p build/books
for book in mt mk lk jn 1cor 2cor 1jn 2jn 3jn; do
  pandoc text/$book??.md --lua-filter code/preprocess.lua --reference-location=block -o build/books/$book.md
done
python3 code/mkRTFnotes.py build/books/*.md --lua-filter code/accRTFworkarounds.lua --template template.rtf --resource-path .:text -o build/notes
