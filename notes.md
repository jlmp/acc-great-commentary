

# Input conversion process

## Convert to Markdown

The HTML is in Windows-1252 encoding, but doesn't indicate it in the header. So do

    curl 'http://www.catholicapologetics.info/scripture/newtestament/8matth.htm' | iconv -f cp1252 -t utf-8 | pandoc -f html -o raw/mt08.md
    cat raw/mt08.md | code/clean-md.py "Matthew 8" > text/mt08.md

The cleanup script will handle the search-and-replace work except for
`\b0` and the check for misplaced asterisks. It will also mark verse
references indicated in the text and will mark Scripture references (but
only where there is a complete reference with book, chapter and verse).

## Cleanup with search and replace:

* `\*([.,:;?!])([\s()])` -> `$1*$2`
* `i.e*.,` -> `i.e.,*`     (not a regex search)
* `\b0` (whole word or followed by letter) -> O
* Check for misplaced asterisks: `pandoc text/mt08.md --lua-filter code/check-md.lua -t markdown -o /dev/null`
    * This helps correct some: `(\b[xlvi]+[,.])\* ` -> `$1 *`.
    * It's better to not include "c" in the regex, because "c." for chapter is common.

## Mark verse references indicated in text

* `(ver\.\s(\d+))` -> `[$1]{.vref ref="2Cor. 12:$2"}` Check manually to see if the link would be correct, and perhaps use a case-sensitive search to avoid matching the verse number that marks the beginning of commentary.

Replace `^(Ver.\s+(\d+))` with `### Luke 1:$2\n\n$1`, changing book
name and chapter number manually.

* Formerly `^Ver.\s+(\d+)\.?[\s-]+` -> `### Luke 1:$1\n\n`: this
eliminated the "Ver. \d+", which initially seemed useless. However,
there are some chapters in which the Vulgate verse numbering differs
from the ESV (e.g., John 6). There I've used headings with verse numbers
that correspond to the ESV, and so the indication of the verse number
from the printed book serves to show the Vulgate correspondence.

Commentary for multiple verses uses "Vers." instead of "Ver." There are
also other variations. Mt 1–2 contained many unlabeled verses.

## Mark Scripture references

Regex for marking Scripture references:

* `(([1-4]\.?\s+)?(S\.?\s+)?[A-Z]\w+\.?\s+[clxvi]+\.?\s+\d+)` -> [$1]{.vref}
* `(([1-4]\.?\s+)?(S\.?\s+)?[A-Z]\w+\.?\s+[clxvi]+\.?\s+[\d-,\s]*\d)`

The latter allows verse ranges and lists. Try it first.

## Page numbers

These must be marked manually. The HTML is derived from the fourth edition (London, 1890–). Where I cannot find the fourth edition online, I will link to the third.

## Replace Greek and Hebrew words

Frequently, letters or accents are transcribed incorrectly.

# Issues already dealt with

When producing RTF files with pandoc, use the --standalone switch.

Accordance User Notes import:

* Doesn't automatically recognize Scripture references.
* Links are imported, but an accord:// link is not treated the same as a
  Scripture link made by editing the note. The link has to be a verse
  reference (e.g., "1Cor. 7:5") instead of a URL.
* The following are not supported:
    * footnotes: they are omitted altogether.
    * Small caps: macOS's RTF parser doesn't recognize them, so support
      will not be added. I use a Lua filter to work around this limitation.
    * Images: there's an image in the commentary on Luke 3:23. It's
      dropped.
    * Paragraph indentation and spacing: RTF attributes for paragraph
      indentation and space before and after a paragraph are ignored.