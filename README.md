# The Great Commentary of Cornelius a Lapide in Markdown

With permission, I am converting to Markdown the HTML edition of
Cornelius a Lapide's commentary hosted at
[www.catholicapologetics.info](http://www.catholicapologetics.info/scripture/newtestament/Lapide.htm).
My main purpose is to use it in the Accordance Bible
software program. For this reason, I have omitted the Bible translations
which in print and in the HTML edition precede each chapter.
The permission I have covers the use of this commentary in Accordance by
anyone.

## Status

Eight volumes of the commentary were published before the series
ended. This is a lot of text to edit in one's spare time. Currently,
I have converted about half of the commentary, including all of the
commentary on the Gospels according to Mark and Luke. However, these
are sparely commentated, because passages common to Matthew are treated
in the commentary on his Gospel.

My first goal is to simply convert all the files so that they can be
accessed in Accordance, marking at least some of the verse numbers.
Recently, I've been marking all of them.

Once I have the text divided into verses, I will correct the Greek and
Hebrew words quoted.

To make it easy to check the printed text, I have added page numbers
and linked them to volumes available online in the
Internet Archive. Marking every page number is not a high priority,
but I plan to mark at least the beginning of each chapter's commentary.

I have found and corrected a few errors and important omissions with
respect to the printed translation.

## The commentary

Cornelius had this to say about his commentary on the Gospels:

> Very many, both in ancient and modern times, have written 
> commentaries on the Gospels. …
> 
> Of recent commentators the number is all but infinite. Their 
> superabundance makes it difficult for the reader to know which 
> to choose, so that he might say with Niobe of old, "Abundance 
> has made me poor." 
> 
> For myself, I have written the following commentaries, partly 
> at Louvain, A.D. 1600, partly when I was teaching and lecturing 
> publicly on the Gospels at Rome [1616–1637†]. I am now an old man, and 
> have passed nearly all my life in learning in the school of the 
> Holy Scriptures. In a science so vast, so sublime and difficult, 
> no one ought to be a teacher and doctor until he has spent long 
> time in studying as a disciple of the doctors.

The *Catholic Encyclopedia* article "[Cornelius Cornelii a
Lapide](https://en.wikisource.org/wiki/Catholic_Encyclopedia_(1913)/Cornelius_Cornelii_a_Lapide)"
says that the commentary on the Gospels was published posthumously,
and mentions 16 editions of a Lapide's complete commentaries 
(the [PRDL lists](http://www.prdl.org/author_view.php?a_id=797)
much of what is available online).

> These numerous editions show how highly these works are estimated
> by Catholics. But Protestant voices have joined in the appreciation.
> G. H. Goetzius (Leipzig, 1699) wrote an academical dissertation,
> "Exercitatio theologica de Cornelii a Lapide Commentariis in Sacram
> Scripturam", in which he praises the Jesuit author as the most
> important of Catholic Scriptural writers. An English translation of
> the complete commentaries was undertaken by the Rev. Thomas W.
> Moseman, an Anglican clergyman, …

and that is what is presented here. Although Cornelius commented the
entire Bible except for Job and Psalms, the translation only includes
the Gospels, the two epistles to the Corinthians, that to the Galatians,
and the epistles of St. John. Moseman abridged the translation—except in some
chapters—in the interest of making the commentary less prolix.

The Greek tends to prefer Attic forms, which were presumably considered
the standard in Cornelius's time. Sometimes I am unable to find the
readings he gives in any edition of the Greek New Testament at my
disposition. Perhaps they come from the Complutensian Polyglot.

Since a Lapide's time, many works of the Fathers he cites have been
shown to be by other authors. Let the reader beware of citing them under
the names he gives unless he knows the status of the work in question.
Nevertheless, the discussion of the positions they present is still of
interest, because they are part of the interpretative tradition. Other
fields have also progressed since his time. In particular, much more is
now known about the geography of the Holy Land.

## Importing into Accordance

* Prerequisites:
    * These are instructions for the macOS Terminal.
    * You will need to [install pandoc](https://pandoc.org/installing.html) if you have not already done so.
    * The `preprocess.lua` filter uses the luarock `luautf8`. These terminal commands will install it using [homebrew](https://brew.sh):

            brew install luarocks
            luarocks install luautf8

* Download this repository using `git` or the download icon on the [repository page](https://gitlab.com/jlmp/acc-great-commentary). Unpack the archive in the latter case.
* Run the command `./build.sh` in the directory that contains it.
* When the process completes, use File > User Files > Import Notes Folder… in Accordance to import the RTF files from `build/notes`.
    * If it is not the first time, you will probably want to delete the notes from the previous import first so that you can give the new set of notes the same name.

## How the code works

* There is a Markdown file for each chapter in the `text` subdirectory.
* The first pass, which is executed for each commented book, reads all the chapter files for the book with pandoc, runs two Lua filters, and outputs the book commentary as a single Markdown file in `build/books`.
    * The first filter is `code/preprocess.lua`. This turns spans with the `.vref` class into links to the corresponding passage of Scripture, and turns spans with the `.pnum` class into links to pages of scanned books hosted on the Internet.
        * It expects Scripture references in a nineteenth century style: "John iii. 16."
        * This filter also checks for page numbers out of sequence. This is one reason for processing a whole book of Scripture instead of just one chapter. The warning "page 294 follows page 295" does not indicate a real problem. It is produced because both page numbers are in the same paragraph, and in this case the Pandoc filter traversal order does not match reading order.
    * The second filter is `code/accRTFworkarounds.lua`, which tries to work around some limitations of Accordance RTF import. For example, small caps are not supported, so spans with the `.smallcaps` class are turned into all caps.
* The second pass reads the Markdown file for a book and outputs an RTF file for each commented verse. This is done by `code/mkRTFnotes.py`, which splits the file at third level headings (`###`), converting the Markdown that follows the heading into an appropriately named RTF file in `code/notes`.
    * The script runs pandoc once per verse to convert Markdown to RTF.
    * The regular expression that matches the verse reference only takes the first verse number into consideration. So if the header is `### 1John 4:3-4`, it will match "1John 4:3" and create `build/notes/1John 4_3.rtf`. This is good because Accordance does not support a User Note on a range of verses.
    * The `-t` switch can be used to supply an RTF template to pandoc. This can be used to set the font and size for the notes.
    * The `-p` switch can be used to supply the path for resources, like images. The images will be added to the RTF files, but lost on import because Accordance's User Notes import does not support them.

If you want to use these scripts for another project, you are welcome to
do so, but I do not warrant that they will be suitable. This is code I
wrote for my own use for this project. It does not have as much error
checking as it should (e.g., if there are two comments on the same
verse, the second will silently overwrite the first when the RTF files
are created). The scripts for the first phase are specific to the format
this commentary, and are likely to need modification for other projects.
The script for the second pass should be easier to reuse.

## Formatting the Markdown input

* The best way to understand this at present is to look at the files in
  the `text` directory.
* There are three levels of headings: book, chapter, and verse(s).
    * `mkRTFnotes.py` splits the files into verse files at third level
       headings, as noted above.
* I use two scripts to help with the process of converting the original
  HTML to Markdown.
    * I use `code/clean-md.py` to automate some reformatting tasks that
      I formerly did manually. One of these is tagging Scripture
      references.
    * The Lua filter `check-md.lua` warns about asterisks in the
      formatted text, which are usually a sign of an error in marking
      italics.
